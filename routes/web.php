<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/**
 * Route for home page
 */
Route::get('/', [
    'uses' => 'BandController@getIndex',
    'as' => 'home'
]);

/**
 * Routes used for Band pages
 */
Route::group(['prefix' => 'band'], function () {
    Route::get('/', [
        'uses' => 'BandController@getIndex',
        'as' => 'band.index'
    ]);

    Route::get('/create', [
        'uses' => 'BandController@getCreate',
        'as' => 'band.create'
    ]);

    Route::post('/create', [
        'uses' => 'BandController@postCreate',
        'as' => 'band.create'
    ]);

    Route::get('/edit/{id}', [
        'uses' => 'BandController@getEdit',
        'as' => 'band.edit'
    ]);

    Route::put('/edit/{id}', [
        'uses' => 'BandController@putEdit',
        'as' => 'band.edit'
    ]);

    Route::get('/delete/{id}', [
        'uses' => 'BandController@getDelete',
        'as' => 'band.delete'
    ]);
});

/**
 * Routes used for album pages
 */
Route::group(['prefix' => 'album'], function () {
    Route::get('/', [
        'uses' => 'AlbumController@getIndex',
        'as' => 'album.index'
    ]);

    Route::get('/create', [
        'uses' => 'AlbumController@getCreate',
        'as' => 'album.create'
    ]);

    Route::post('/create', [
        'uses' => 'AlbumController@postCreate',
        'as' => 'album.create'
    ]);

    Route::get('/edit/{id}', [
        'uses' => 'AlbumController@getEdit',
        'as' => 'album.edit'
    ]);

    Route::put('/edit/{id}', [
        'uses' => 'AlbumController@putEdit',
        'as' => 'album.edit'
    ]);

    Route::get('/delete/{id}', [
        'uses' => 'AlbumController@getDelete',
        'as' => 'album.delete'
    ]);
});
