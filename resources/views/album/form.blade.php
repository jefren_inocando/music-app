@if (count($errors) > 0)
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {!! Form::label('name','Name',['class'=>'control-label']) !!}
    {!! Form::text('name', $album->name,['class'=>'form-control', 'required'=>'required']) !!}
</div>

<div class="form-group {{ $errors->has('band_id') ? 'has-error' : '' }}">
    {!! Form::label('band_id','Band',['class'=>'control-label']) !!}
    {!! Form::select('band_id', $bandList, $album->band_id,['class'=>'form-control', 'required'=>'required']) !!}
</div>

<div class="form-group {{ $errors->has('recorded_date') ? 'has-error' : '' }}">
    {!! Form::label('recorded_date','Recorded Date',['class'=>'control-label']) !!}
    {!! Form::date('recorded_date', $album->recorded_date,['class'=>'form-control']) !!}
</div>

<div class="form-group {{ $errors->has('release_date') ? 'has-error' : '' }}">
    {!! Form::label('release_date','Release Date',['class'=>'control-label']) !!}
    {!! Form::date('release_date', $album->release_date,['class'=>'form-control']) !!}
</div>

<div class="form-group {{ $errors->has('number_of_tracks') ? 'has-error' : '' }}">
    {!! Form::label('number_of_tracks','Number of tracks',['class'=>'control-label']) !!}
    {!! Form::text('number_of_tracks', $album->number_of_tracks,['class'=>'form-control']) !!}
</div>

<div class="form-group {{ $errors->has('label') ? 'has-error' : '' }}">
    {!! Form::label('label','Label',['class'=>'control-label']) !!}
    {!! Form::text('label', $album->label,['class'=>'form-control']) !!}
</div>

<div class="form-group {{ $errors->has('producer') ? 'has-error' : '' }}">
    {!! Form::label('producer','Producer',['class'=>'control-label']) !!}
    {!! Form::text('producer', $album->producer,['class'=>'form-control']) !!}
</div>

<div class="form-group {{ $errors->has('genre') ? 'has-error' : '' }}">
    {!! Form::label('genre','Genre',['class'=>'control-label']) !!}
    {!! Form::text('genre', $album->genre,['class'=>'form-control']) !!}
</div>