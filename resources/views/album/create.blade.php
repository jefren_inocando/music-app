@extends('layouts.master')

@section('title')
    Create Album
@endsection

@section('content')

    <div class="col-sm-12">
        {!! Form::model($album, ['route' => ['album.create'], 'method' => 'post']) !!}
        @include('album.form')
        {!! Form::submit('Save',['class' => 'btn btn-default']) !!}
        {!! Form::close() !!}
    </div>

@endsection