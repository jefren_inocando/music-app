@extends('layouts.master')

@section('title')
    Album List
@endsection

@section('content')

    <div class="row">
        <a href="{{ route('album.create') }}" class="btn btn-default btn-lg">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        </a>
    </div>

    <div class="row">
        <div class="form-group">
            {!! Form::label('searchByBand','Search by Band',['class'=>'control-label']) !!}
            {!! Form::select('searchByBand', $bandList, $searchByBand,['class'=>'form-control', 'id'=>'searchByBand']) !!}
        </div>
    </div>

    <div class="row">

        <table class="table">
            <thead>
            <tr>
                <th><a href="{{ Request::fullUrlWithQuery(['orderBy' => 'name']) }}">Name</a></th>
                <th><a href="{{ Request::fullUrlWithQuery(['orderBy' => 'band_id']) }}">Band</a></th>
                <th><a href="{{ Request::fullUrlWithQuery(['orderBy' => 'recorded_date']) }}">Recorded Date</a></th>
                <th><a href="{{ Request::fullUrlWithQuery(['orderBy' => 'release_date']) }}">Release Date</a></th>
                <th><a href="{{ Request::fullUrlWithQuery(['orderBy' => 'number_of_tracks']) }}">Number of Tracks</a></th>
                <th><a href="{{ Request::fullUrlWithQuery(['orderBy' => 'label']) }}">Label</a></th>
                <th><a href="{{ Request::fullUrlWithQuery(['orderBy' => 'producer']) }}">Producer</a></th>
                <th><a href="{{ Request::fullUrlWithQuery(['orderBy' => 'genre']) }}">Genre</a></th>
                <th></th>
            </tr>
            </thead>

            <tbody>
            @foreach($albums as $album)

                <tr>
                    <td>{{ $album->name }}</td>
                    <td>{{ $album->band->name }}</td>
                    <td>{{ !empty($album->recorded_date) ? date('m/d/Y',strtotime($album->recorded_date)) : '' }}</td>
                    <td>{{ !empty($album->recorded_date) ? date('m/d/Y',strtotime($album->release_date)) : '' }}</td>
                    <td>{{ $album->number_of_tracks }}</td>
                    <td>{{ $album->label }}</td>
                    <td>{{ $album->producer }}</td>
                    <td>{{ $album->genre }}</td>
                    <td>
                        <a href="{{ route('album.edit', $album->id) }}">
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </a>
                        <a href="{{ route('album.delete', $album->id) }}">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </a>
                    </td>
                </tr>

            @endforeach
            </tbody>

        </table>

    </div>

    <div class="row">
        <div>{{ $albums->render() }}</div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready( function() {
            $('#searchByBand').change( function() {
                location.href = 'album?searchByBand=' + $(this).val();
            });
        });
    </script>
@endsection