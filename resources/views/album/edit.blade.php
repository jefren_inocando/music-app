@extends('layouts.master')

@section('title')
    Edit Album
@endsection

@section('content')

    <div class="col-sm-12">

        {!! Form::model($album, ['route' => ['album.edit', $album->id], 'method' => 'put']) !!}
        @include('album.form')
        {!! Form::submit('Save',['class' => 'btn btn-default']) !!}
        {!! Form::close() !!}

    </div>

@endsection