@extends('layouts.master')

@section('title')
    Edit band
@endsection

@section('content')

    <div class="col-sm-12">

        {!! Form::model($band, ['route' => ['band.edit', $band->id], 'method' => 'put']) !!}
        @include('band.form')
        {!! Form::submit('Save',['class' => 'btn btn-default']) !!}
        {!! Form::close() !!}

    </div>

@endsection