@extends('layouts.master')

@section('title')
    Create Band
@endsection

@section('content')

    <div class="col-sm-12">
        {!! Form::model($band, ['route' => ['band.create'], 'method' => 'post']) !!}
        @include('band.form')
        {!! Form::submit('Save',['class' => 'btn btn-default']) !!}
        {!! Form::close() !!}
    </div>

@endsection