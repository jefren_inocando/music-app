@if (count($errors) > 0)
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    {!! Form::label('name','Name',['class'=>'control-label']) !!}
    {!! Form::text('name', $band->name,['class'=>'form-control', 'required'=>'required']) !!}
</div>

<div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}">
    {!! Form::label('start_date','Start Date',['class'=>'control-label']) !!}
    {!! Form::date('start_date', $band->recorded_date,['class'=>'form-control']) !!}
</div>

<div class="form-group {{ $errors->has('website') ? 'has-error' : '' }}">
    {!! Form::label('website','Website',['class'=>'control-label']) !!}
    {!! Form::text('website', $band->website,['class'=>'form-control']) !!}
</div>

<div class="form-group {{ $errors->has('still_active') ? 'has-error' : '' }}">
    {!! Form::label('still_active','Still Active',['class'=>'control-label']) !!}
    {!! Form::select('still_active', [1 => 'Yes', 0 => 'No'], $band->still_active, ['class'=>'form-control']) !!}
</div>