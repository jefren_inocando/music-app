@extends('layouts.master')

@section('title')
    Band List
@endsection

@section('content')

    <div class="row">
        <a href="{{ route('band.create') }}" class="btn btn-default btn-lg">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        </a>
    </div>

    <div class="row">

        <table class="table">
            <thead>
            <tr>
                <th><a href="{{ Request::fullUrlWithQuery(['orderBy' => 'name']) }}">Name</a></th>
                <th><a href="{{ Request::fullUrlWithQuery(['orderBy' => 'start_date']) }}">Start Date</a></th>
                <th><a href="{{ Request::fullUrlWithQuery(['orderBy' => 'website']) }}">Website</a></th>
                <th><a href="{{ Request::fullUrlWithQuery(['orderBy' => 'still_active']) }}">Active</a></th>
                <th>Albums</th>
                <th></th>
            </tr>
            </thead>

            <tbody>
            @foreach($bands as $band)

                <tr>
                    <td>{{ $band->name }}</td>
                    <td>{{ $band->start_date }}</td>
                    <td>{{ $band->website }}</td>
                    <td>{{ $band->still_active ? 'Yes' : 'No' }}</td>
                    <td>
                        <ul>
                            @foreach($band->albums as $album)
                                <li><a href="{{ route('album.edit', $album->id) }}">{{ $album->name }}</a></li>
                            @endforeach
                        </ul>
                    </td>
                    <td>
                        <a href="{{ route('band.edit', $band->id) }}">
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </a>
                        <a href="{{ route('band.delete', $band->id) }}">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </a>
                    </td>
                </tr>

            @endforeach
            </tbody>

        </table>

    </div>

    <div class="row">
        <div>{{ $bands->render() }}</div>
    </div>

@endsection