<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'albums';
    protected $guarded = ['id'];

    /**
     * Formats date and check null values
     * @param $value
     */
    public function setRecordedDateAttribute($value)
    {
        $this->attributes['recorded_date'] = !empty($value) ? date('Y-m-d', strtotime($value)) : null;
    }

    /**
     * Formats date and check null values
     * @param $value
     */
    public function setReleaseDateAttribute($value)
    {
        $this->attributes['release_date'] = !empty($value) ? date('Y-m-d', strtotime($value)) : null;
    }

    /**
     * Sets value to zero if null
     * @param $value
     */
    public function setNumberOfTracksAttribute($value)
    {
        $this->attributes['number_of_tracks'] = !empty($value) ? $value : 0;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function band()
    {
        return $this->belongsTo('App\Models\Band');
    }
}
