<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AlbumRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'band_id' => 'required',
            'recorded_date' => 'date',
            'release_date' => 'date',
            'number_of_tracks' => 'numeric',
            'label' => 'regex:/^[a-zA-Z0-9 ]+$/',
            'producer' => 'regex:/^[a-zA-Z0-9 ]+$/',
            'genre' => 'regex:/^[a-zA-Z0-9 ]+$/',
        ];
    }
}
