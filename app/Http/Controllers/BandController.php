<?php

namespace App\Http\Controllers;

use App\Http\Requests\BandRequest;
use App\Models\Band;
use App\Repositories\BandRepository;
use Illuminate\Http\Request;

class BandController extends Controller
{

    protected $bandModel;

    public function __construct(BandRepository $bandRepository)
    {
        $this->bandModel = $bandRepository;
    }

    /**
     * Loads all Bands
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function getIndex(Request $request)
    {
        $orderBy = $request->input('orderBy');
        $bands = $this->bandModel->index(5, $orderBy);
        return view('band.index', ['bands' => $bands]);
    }

    /**
     * Deletes a band
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id, Request $request)
    {
        $this->bandModel->delete($id);
        $request->session()->flash('message', 'Successfully deleted');
        return redirect()->route('band.index');
    }

    /**
     * Displays create form
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function getCreate()
    {
        $band = new Band();
        return view('band.create', [
            'band' => $band
        ]);
    }

    /**
     * Creates a new band
     * @param BandRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(BandRequest $request)
    {
        if ($this->bandModel->store($request->input())) {
            $request->session()->flash('message', 'Successfully saved');
        }

        return redirect()->route('band.index');
    }

    /**
     * Displays edit form
     * @param $id
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function getEdit($id)
    {
        $band = $this->bandModel->getById($id);
        return view('band.edit', ['band' => $band]);
    }

    /**
     * Updates a band
     * @param $id
     * @param BandRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function putEdit($id, BandRequest $request)
    {
        if ($this->bandModel->update($id, $request->input())) {
            $request->session()->flash('message', 'Successfully saved');
        }

        return redirect()->route('band.index');
    }
}
