<?php

namespace App\Http\Controllers;

use App\Http\Requests\AlbumRequest;
use App\Models\Album;
use App\Repositories\AlbumRepository;
use App\Repositories\BandRepository;
use Illuminate\Http\Request;

class AlbumController extends Controller
{

    protected $albumModel;
    protected $bandModel;

    public function __construct(AlbumRepository $albumRepository, BandRepository $bandRepository)
    {
        $this->albumModel = $albumRepository;
        $this->bandModel = $bandRepository;
    }

    /**
     * Displays albums
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function getIndex(Request $request)
    {
        $searchByBand = $request->input('searchByBand');
        $orderBy = $request->input('orderBy');
        $albums = $this->albumModel->index(5, $searchByBand, $orderBy);
        $bandList = $this->bandModel->getBandList();
        return view('album.index', [
            'albums' => $albums,
            'bandList' => $bandList,
            'searchByBand' => $searchByBand,
        ]);
    }

    /**
     * Deletes an album
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id, Request $request)
    {
        $this->albumModel->delete($id);
        $request->session()->flash('message', 'Successfully deleted');
        return redirect()->route('album.index');
    }

    /**
     * Displays create form
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function getCreate()
    {
        $album = new Album();
        $bandList = $this->bandModel->getBandList();
        return view('album.create', [
            'album' => $album,
            'bandList' => $bandList,
        ]);
    }

    /**
     * Creates a new album
     * @param AlbumRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCreate(AlbumRequest $request)
    {
        if ($this->albumModel->store($request->input())) {
            $request->session()->flash('message', 'Successfully saved');
        }

        return redirect()->route('album.index');
    }

    /**
     * Displays edit form
     * @param $id
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function getEdit($id)
    {
        $album = $this->albumModel->getById($id);
        $bandList = $this->bandModel->getBandList();
        return view('album.edit', [
            'album' => $album,
            'bandList' => $bandList,
        ]);
    }

    /**
     * Updates an album
     * @param $id
     * @param AlbumRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function putEdit($id, AlbumRequest $request)
    {
        if ($this->albumModel->update($id, $request->input())) {
            $request->session()->flash('message', 'Successfully saved');
        }

        return redirect()->route('album.index');
    }
}
