<?php
namespace App\Repositories;


use App\Models\Band;

class BandRepository extends BaseRepository
{

    /**
     * BandRepository constructor.
     * @param Band $band
     */
    public function __construct(Band $band)
    {
        $this->model = $band;
    }


    /**
     * Query all bands with pagination
     *
     * @param $num
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index($num, $orderBy)
    {
        $band = $this->model->with('albums');

        if (!empty($orderBy)) {
            $band->orderBy($orderBy);
        }

        return $band->paginate($num);
    }

    /**
     * Returns an array of bands
     *
     * @return mixed
     */
    public function getBandList()
    {
        return $this->model->pluck('name', 'id')->prepend('', '')->all();
    }

    /**
     * Creates a new band
     * @param $data
     * @return mixed
     */
    public function store($data)
    {
        $band = new $this->model;
        $band->fill($data);
        return $band->save();
    }

    /**
     * Updates a band
     * @param $id
     * @param $data
     * @return mixed
     */
    public function update($id, $data)
    {
        $band = $this->getById($id);
        $band->fill($data);
        return $band->save();
    }

}