<?php
namespace App\Repositories;

use App\Models\Album;

class AlbumRepository extends BaseRepository
{

    /**
     * AlbumRepository constructor.
     * @param Album $album
     */
    public function __construct(Album $album)
    {
        $this->model = $album;
    }

    /**
     * Query all albums with pagination
     *
     * @param $num
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index($num, $searchByBand, $orderBy)
    {
        $album = $this->model->with('band');

        if (!empty($searchByBand)) {
            $album->where('band_id', $searchByBand);
        }

        if (!empty($orderBy)) {
            $album->orderBy($orderBy);
        }

        return $album->paginate($num);
    }

    /**
     * Creates a new album
     * @param $data
     * @return mixed
     */
    public function store($data)
    {
        $album = new $this->model;
        $album->fill($data);
        return $album->save();
    }

    /**
     * Updates an album
     * @param $id
     * @param $data
     * @return mixed
     */
    public function update($id, $data)
    {
        $album = $this->getById($id);
        $album->fill($data);
        return $album->save();
    }

}