<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlbumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('albums', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('band_id')->unsigned();
            $table->string('name', 255);
            $table->date('recorded_date')->nullable();
            $table->date('release_date')->nullable();
            $table->integer('number_of_tracks')->unsigned()->default(0);
            $table->string('label', 255)->nullable();
            $table->string('producer', 255)->nullable();
            $table->string('genre', 255)->nullable();
            $table->timestamps();
        });

        Schema::table('albums', function(Blueprint $table) {
            $table->foreign('band_id')->references('id')->on('bands')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('albums', function(Blueprint $table) {
            $table->dropForeign('albums_band_id_foreign');
        });

        Schema::drop('albums');
    }
}
