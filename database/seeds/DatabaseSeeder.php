<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Band;
use App\Models\Album;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Band::create([
            'name' => 'Bruno Mars',
        ]);

        Album::create([
            'band_id' => 1,
            'name' => '24K Magic',
        ]);

        Album::create([
            'band_id' => 1,
            'name' => 'Unorthodox Jukebox',
        ]);
    }
}
